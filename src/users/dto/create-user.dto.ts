import { IsNotEmpty, MinLength, Matches } from 'class-validator';
export class CreateUserDto {
  @IsNotEmpty()
  @MinLength(5)
  login: string;

  @IsNotEmpty()
  @MinLength(5)
  name: string;

  @Matches(/^(?=.*[0-9])(?=.*[!@#$%^&*])[a-zA-Z0-9!@#$%^&*]{6,16}$/)
  @MinLength(8)
  @IsNotEmpty()
  password: string;
}
